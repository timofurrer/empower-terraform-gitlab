terraform {
  backend "http" {}
}

resource "random_password" "main" {
  length = 10
}

output "password" {
  value     = random_password.main.result
  sensitive = true
}